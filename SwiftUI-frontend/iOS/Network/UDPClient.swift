//

import Foundation
import Network

class UDPClient {
    
    enum ConnectionError: Error {
        case invalidIPAdress
        case invalidPort
    }
    
    // Properties
    
    private lazy var queue = DispatchQueue.init(label: "udp.client.queue")
    
    private var connection: NWConnection?
    
    private var state: NWConnection.State = .preparing
    
    // Trying to connect to the server with the server ip address and its port
    
    func connect(to ipAddress: String, with port: UInt16) throws {
        guard let ipAddress = IPv4Address(ipAddress) else {
            throw ConnectionError.invalidIPAdress
        }
        guard let port = NWEndpoint.Port.init(rawValue: port) else {
            throw ConnectionError.invalidPort
        }
        let host = NWEndpoint.Host.ipv4(ipAddress)
        
        connection = NWConnection(host: host, port: port, using: .udp)
        
        connection?.stateUpdateHandler = { [unowned self] state in
            self.state = state
        }
        
        connection?.start(queue: queue)
    }

    // For sending data
    
    func send(data: Data) {
        guard state == .ready else { return }
        
        connection?.send(content: data,
                         completion: .contentProcessed({ error in
            if let error = error {
                print(error)
            }
        }))
    }
}
