//

import Foundation
import Network

class TCPServer {
    enum ServerError: Error {
        case invalidPortNumber
    }
    
    // MARK: - properties
    
    lazy var listeningQueue = DispatchQueue.init(label: "tcp_server_queue")
    lazy var connectionQueue = DispatchQueue.init(label: "connection_queue")
    private var connection: NWConnection?
    
    var listener: NWListener?
    
    var recievedDataHandling: ((Data) -> Void)?
    private var state: NWConnection.State = .preparing
    // MARK: - methods
    
    /// start listening on the port specified
    func start(port: UInt16) throws {
        listener?.cancel()
        
        guard let port = NWEndpoint.Port.init(rawValue: port) else {
            throw ServerError.invalidPortNumber
        }
        
        listener = try NWListener.init(using: .udp, on: port)
                
        listener?.stateUpdateHandler = { state in
            if state == .ready {
                print("listener is ready to recieve data")
            }
        }
        
        listener?.newConnectionHandler = { [unowned self] connection in
            print("connection requested --> \(connection.endpoint)")
            
            connection.start(queue: connectionQueue)
        }
        
        listener?.start(queue: listeningQueue)
    }
    
    func send(data: Data) {
        guard state == .ready else { return }
        
        connection?.send(content: data,
                         completion: .contentProcessed({ error in
            if let error = error {
                print(error)
            }
        }))
    }
}
