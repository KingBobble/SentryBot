//

import Foundation
import AVFoundation

// Abstract: An object to capture video data, encode it and finally send it to the server
class VideoClient {
    
    // Dependencies
    
    private lazy var captureManager = VideoCaptureManager()
    private lazy var videoEncoder = H264Encoder()
    private lazy var udpClient = UDPClient()
    
    // Interface for connecting to the server
    
    func connect(to ipAddress: String, with port: UInt16) throws {
        try udpClient.connect(to: ipAddress, with: port)
    }
    
    // Interface for starting sending video data
    
    func startSendingVideoToServer() throws {
        try videoEncoder.configureCompressSession()
        
        captureManager.setVideoOutputDelegate(with: videoEncoder)
        
        // if connection is not established, 'send(:)' method in UDPClient doesn't
        // have any effect so it's okay to send data before establishing connection
        videoEncoder.naluHandling = { [unowned self] data in
            udpClient.send(data: data)
        }
    }
}
