//

import SwiftUI

struct VideoClientView: View {
    @State private var searchText = ""
    let videoClient = VideoClient()
    
    var body: some View {
        HStack {
            Spacer()
            
            TextField("", text: $searchText, prompt: Text("Enter video server IP address...").foregroundColor(.white))
                .padding(5)
                .padding(.leading, -5)
                .background(Color(red: 0.1922, green: 0.1961, blue: 0.2000))
                .tint(.yellow)
            
            Button {
                if searchText != "" {
                    do {
                        try videoClient.connect(to: searchText, with: 12005)
                        try videoClient.startSendingVideoToServer()
                    } catch {
                        print("error occured : \(error.localizedDescription)")
                    }
                }
            } label : {
                    Image(systemName: "arrowshape.right.circle.fill")
                        .foregroundColor(.blue)
            }
            .padding([.trailing], 10)
        }
    }
}

#Preview {
    VideoClientView()
}
