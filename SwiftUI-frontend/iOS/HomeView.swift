//

import SwiftUI

struct HomeView: View {
    var body: some View {
        NavigationView {
            NavigationLink {
                VideoClientView()
                    .navigationBarHidden(false)
            } label: {
                Image(systemName: "arrowtriangle.forward.circle.fill")
                    .font(.system(size: 25))
                    .padding()
            }
        }
    }
}

#Preview {
    HomeView()
}
