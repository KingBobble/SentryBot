//

import SwiftUI

@main
struct SentryBotApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
