import Stream from '@/components/stream'

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col justify-between px-3 py-3 overflow-hidden">
      <div className="grid text-left grid-cols-2 lg:mb-0 lg:max-w-5xl lg:w-full">
        <Stream category="Video Stream"
          desc="Displays real-time video stream with object detection."
          streamType="video"
        />
        <Stream category="Lidar 2D Mapping"
          desc="Displays real-time distance measurements on a polar map."
          streamType="lidar"
        />
      </div>
    </main>
  );
}
