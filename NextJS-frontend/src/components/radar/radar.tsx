import * as d3 from "d3";
import { INNER_RADIUS, RadarGrid } from "./radar-grid";
import React from "react";
import { polarToCartesian, axisLayoutConfig } from '../helper';

const MARGIN = 30;

type YScale = d3.ScaleRadial<number, number, never>;

/*
  A react component that builds a Radar Chart for several groups in the dataset
*/
export const Radar = ({ width, height, data, axisConfig }) => {
  if (data == null) {
    return null
  }

  const outerRadius = Math.min(width, height) / 2 - MARGIN;
  // The x scale provides an angle for each variable of the dataset
  const allVariableNames = axisConfig.map((axis) => axis.name);
  const xScale = d3
    .scaleBand()
    .domain(allVariableNames)
    .range([0, 2 * Math.PI]);

  // Compute the y scales: 1 scale per variable.
  // Provides the distance to the center.
  let yScales: { [name: string]: YScale } = {};
  axisConfig.forEach((axis) => {
    yScales[axis.name] = d3
      .scaleRadial()
      .domain([0, axis.max])
      .range([INNER_RADIUS, outerRadius]);
  });

  // Compute the main radar shapes, 1 per group
  const lineGenerator = d3.lineRadial();

  // Compute scatter outline plot
  const allAxisVariableNames = axisLayoutConfig().map((axis) => axis.name);
  const axisLayoutxScale = d3
    .scaleBand()
    .domain(allAxisVariableNames)
    .range([0, 2 * Math.PI]);
  
  const allCoordinatesScatterPlot = axisConfig.map((axis, i) => {
    const yScale = yScales[axis.name];
    const angle = xScale(axis.name) ?? 0;
    const radius = yScale(data[axis.name]);
    const coordinate = polarToCartesian(angle, radius);

    return (
      <circle
        key={i}
        r={0.5}
        cx={coordinate.x}
        cy={coordinate.y}
        opacity={0.5}
        stroke="#166ff5"
        fill="#166ff5"
        fillOpacity={0.3}
        strokeWidth={1} />)
  });

  const allCoordinates = axisConfig.map((axis, i) => {
    const yScale = yScales[axis.name];
    const angle = xScale(axis.name) ?? 0;
    const radius = yScale(data[axis.name]);
    const coordinate: [number, number] = [angle, radius];

    return coordinate;
  });

  // To close the path of each group, the path must finish where it started
  // so add the last data point at the end of the array
  allCoordinates.push(allCoordinates[0]);
  const linePath = lineGenerator(allCoordinates);

  return (
    <svg width={width} height={height}>
      <g transform={"translate(" + width / 2 + "," + height / 2 + ")"}>
        <RadarGrid
          outerRadius={outerRadius}
          xScale={axisLayoutxScale}
          axisConfig={axisLayoutConfig()} />
        {allCoordinatesScatterPlot}
        {<path
          d={linePath}
          stroke={"#1dd18c"}
          strokeWidth={1}
          fill={"#1dd18c"}
          fillOpacity={0.3} />}
      </g>
    </svg>
  );
};
