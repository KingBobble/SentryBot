export default function Video() {
    return (
        <img id="video-stream" className="w-auto origin-center" src={"/api/VideoStream"} alt="Error: No Video Output" />
    )
}
