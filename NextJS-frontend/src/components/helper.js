export const polarToCartesian = (angle, distance) => {
    const x = distance * Math.cos(angle)
    const y = distance * Math.sin(angle)
    
    return { x, y }
}

export function axisDefaultConfig(num) {
    const data = []

    if (num == null) {
        return null
    }

    for (let i = 0; i < 2000; i++) {
        data.push({ name: num[i]["angle"].toString(), max: 5000 })
    }

    return data
}

export function lidarDataConfig(num) {
    const data = {}

    if (num == null) {
        return null
    }

    for (let i = 0; i < 2000; i++) {
        data[num[i]["angle"].toString()] = num[i]["distance"]
    }

    return data
}

export function axisLayoutConfig() {
    const data = []

    for (let i = 0; i < 4; i++) {
        data.push({ name: (270-i*90).toString(), max: 5000 })
    }

    return data
}
