import { useState, useEffect } from 'react'
import { Radar } from './radar/radar'
import { axisDefaultConfig, lidarDataConfig } from './helper'

export default function Lidar() {
    const [lidarData, setLidarData] = useState(null)
    var status = false

    useEffect(() => {
        if (lidarData == null && !status) {
            fetch('/api/GetAllLidarDataInfo')
                .then((res) => res.json())
                .then((json) => {
                    setLidarData(json.data.results)
                })
                .catch(err => {
                    console.log(err);
                })
            status = true
        }
    }, [])

    return (
        <div className='overflow-x-scroll'>
            <Radar
                data={lidarDataConfig(lidarData)}
                width={400}
                height={400}
                axisConfig={axisDefaultConfig(lidarData)} />
        </div>
    )
}







