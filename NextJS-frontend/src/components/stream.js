'use client'
import Video from './video'
import Lidar from './lidar'

export default function Stream({ category, desc, streamType }) {

    function rotateVideo(e) {
        const buttonStatus = e.target.id
        var elem
        if (buttonStatus == "video") {
            elem = document.getElementById("video-stream")
        } else {
            return null
        }

        if (elem.classList.contains("rotate-90")) {
            elem.classList.remove("rotate-90")
            elem.classList.add("rotate-180")
        } else if (elem.classList.contains("rotate-180")) {
            elem.classList.remove("rotate-180")
            elem.classList.add("rotate-[270deg]")
        } else if (elem.classList.contains("rotate-[270deg]")) {
            elem.classList.remove("rotate-[270deg]")
            elem.classList.add("rotate-0")
        } else {
            elem.classList.remove("rotate-0")
            elem.classList.add("rotate-90")
        }
    }

    return (
        <div className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30">
            <h2 className={`mb-3 text-2xl font-semibold`}>
                {category}{' '}
                <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
                    <button id={streamType} onClick={rotateVideo}>-&gt;
                    </button>
                </span>
            </h2>
            <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
                {desc}
            </p>
            {streamType == "lidar" ? <Lidar />
                : streamType == "video" ? <Video />
                    : null}
        </div>
    )
}