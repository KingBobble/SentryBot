/** @type {import('next').NextConfig} */
/**
const nextConfig = {};

export default nextConfig;
*/

module.exports = {
    async rewrites() {
        return [
            {
                source: '/api/:path*',
                destination: process.env.API_HOST+'/api/:path*', // Proxy to Backend
            },
        ]
    },
}
