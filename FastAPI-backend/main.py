import uvicorn
import logging
import cv2
import time
from fastapi import FastAPI, HTTPException
from fastapi.responses import StreamingResponse
from fastapi.staticfiles import StaticFiles
import threading
import queue
from config import config_path
import models
import database


app = FastAPI(
    docs_url=None,  # Disable docs API
    redoc_url=None  # Disable redoc API
)
# Read abs paths from config.ini
DATA_PATH = config_path()
UDP_PATH = DATA_PATH["udp_path"]
STATIC_PATH = DATA_PATH["static_path"]

# Create a video buffer queue
video_buf = queue.Queue()


def receive_video_stream():
    status = False

    while True:
        if not status:
            # Web Camera Video Stream
            #cap = cv2.VideoCapture(0)
            # UDP Video Stream 
            cap = cv2.VideoCapture(UDP_PATH, cv2.CAP_FFMPEG)
            cap.set(cv2.CAP_PROP_BUFFERSIZE, 2)
        
        if not cap.isOpened():
            print("Error: VideoCapture is not opened.")
            cap.release()
        else:
            status = True
        
        if status:
            cap.grab()
            cap.grab()
            ret, frame = cap.read()

            if not ret:
                print("Error: Empty frame.")
                status = False
                cap.release()

            video_buf.put(frame)


def gen_frames():
    # importing cascade
    faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")

    while True:
        if not video_buf.empty():
            frame = video_buf.get()

            # converting image from color to grayscale 
            imgGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Getting corners around the face
            # 1.3 = scale factor, 5 = minimum neighbor can be detected
            faces = faceCascade.detectMultiScale(imgGray, 1.3, 5)  

            # drawing bounding box around face
            for (x, y, w, h) in faces:
                frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255,   0), 3)
            
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

        time.sleep(0.001)


@app.get('/api/VideoStream')
def video_stream():
    return StreamingResponse(gen_frames(), media_type='multipart/x-mixed-replace; boundary=frame')


@app.get("/api/GetAllLidarDataInfo")
def get_all_lidar_data_info():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    data_response = database.get_all_data_info()

    if data_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_ERROR"
        returnmessage["messages"].append("Data was not able to be retrieved from the database.")
        raise HTTPException(status_code=400, detail=returnmessage)
    
    json_response = {"results": []}

    for data in data_response:
        data_json = database.convert_simple_data_to_json(data)
        json_response["results"].append(data_json)

    returnmessage["data"] = json_response

    return returnmessage


@app.post("/api/AddDataPoints")
def add_data_points(data: models.DataList):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    status = database.add_data_points(data.data_list)

    if not status:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_ERROR"
        returnmessage["messages"].append("Data not able to be added to the database.")
        raise HTTPException(status_code=400, detail=returnmessage)

    return returnmessage


app.mount("/api", StaticFiles(directory=STATIC_PATH), name="static")


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    database.create_tables()
    
    """Run main program with command: uvicorn main:app --host 127.0.0.1 --port 8000 --workers 4

    main: the file main.py (the Python "module").
    app: the object created inside of main.py with the line app = FastAPI().
    --reload: make the server restart after code changes. Only use for development.
    
    Interactive API documentation: http://127.0.0.1:8000/docs

    Gunicorn + Uvicorn combo command:

    gunicorn main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind 127.0.0.1:8000
    """

    video_thread = threading.Thread(target=receive_video_stream, daemon=True)
    video_thread.start()

    # localhost
    uvicorn.run(app, host="127.0.0.1", port=8000)

    # private ip address
    # uvicorn.run(app, host="0.0.0.0", port=8000)#, forwarded_allow_ips='*', reload=True)
