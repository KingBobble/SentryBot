import cv2
import time
import uvicorn
from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from fastapi.responses import StreamingResponse
from config import config_path


app = FastAPI(
    docs_url=None,  # Disable docs API
    redoc_url=None  # Disable redoc API
)
# Web Camera Video Stream
#camera = cv2.VideoCapture(0)
# Read abs paths from config.ini
DATA_PATH = config_path()
UDP_PATH = DATA_PATH["udp_path"]
TEMPLATE_PATH = DATA_PATH["template_path"]
# UDP Video Stream 
camera = cv2.VideoCapture(UDP_PATH, cv2.CAP_FFMPEG)
camera.set(cv2.CAP_PROP_BUFFERSIZE, 2)
# Set template object
templates = Jinja2Templates(directory=TEMPLATE_PATH)


def gen_frames():
    while True:
        camera.grab()
        camera.grab()
        success, frame = camera.read()
        if not success:
            break
        else:
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        time.sleep(0.001)


@app.get('/')
def index(request: Request):
    return templates.TemplateResponse("index_streaming.html", {"request": request})


@app.get('/video_stream')
def video_stream():
    return StreamingResponse(gen_frames(), media_type='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    # localhost
    uvicorn.run(app, host="127.0.0.1", port=8000)

    # private ip address
    # uvicorn.run(app, host="0.0.0.0", port=8000)#, forwarded_allow_ips='*', reload=True)
