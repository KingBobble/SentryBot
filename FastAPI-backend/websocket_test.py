from fastapi import FastAPI, Request, WebSocket, WebSocketDisconnect
from websockets.exceptions import ConnectionClosed
from fastapi.templating import Jinja2Templates
import uvicorn
import asyncio
import cv2
from config import config_path


app = FastAPI(
    docs_url=None,  # Disable docs API
    redoc_url=None  # Disable redoc API
)
# Web Camera Video Stream
#camera = cv2.VideoCapture(0)
# Read abs paths from config.ini
DATA_PATH = config_path()
UDP_PATH = DATA_PATH["udp_path"]
TEMPLATE_PATH = DATA_PATH["template_path"]
# UDP Video Stream 
camera = cv2.VideoCapture(UDP_PATH,cv2.CAP_FFMPEG)
camera.set(cv2.CAP_PROP_BUFFERSIZE, 2)
# Set template object
templates = Jinja2Templates(directory=TEMPLATE_PATH)


@app.get('/')
def index(request: Request):
    return templates.TemplateResponse("index_ws.html", {"request": request})


@app.websocket("/ws")
async def get_stream(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            camera.grab()
            camera.grab()
            success, frame = camera.read()
            if not success:
                break
            else:
                ret, buffer = cv2.imencode('.webp', frame)
                await websocket.send_bytes(buffer.tobytes())
            await asyncio.sleep(0.001)
    except (WebSocketDisconnect, ConnectionClosed):
        print("Client disconnected")


if __name__ == '__main__':
    # localhost
    uvicorn.run(app, host='127.0.0.1', port=8000)

    # private ip address
    # uvicorn.run(app, host="0.0.0.0", port=8000)#, forwarded_allow_ips='*', reload=True)
