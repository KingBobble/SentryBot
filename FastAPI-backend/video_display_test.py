import cv2
from time import sleep
import threading
import queue
from config import config_path


q = queue.Queue()
DATA_PATH = config_path()
UDP_PATH = DATA_PATH["udp_path"]


def displayVideoStream():
    # importing cascade
    faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
    
    while True:
        if not q.empty():
            frame = q.get()

            # converting image from color to grayscale 
            imgGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Getting corners around the face
            # 1.3 = scale factor, 5 = minimum neighbor can be detected
            faces = faceCascade.detectMultiScale(imgGray, 1.3, 5)  

            # drawing bounding box around face
            for (x, y, w, h) in faces:
                frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255,   0), 3)
            
            cv2.imshow('Video', frame)
    
        if cv2.waitKey(1)&0XFF == ord('q'):
            break

    cv2.destroyAllWindows()


def receiveVideoStream():
    cap = cv2.VideoCapture(UDP_PATH, cv2.CAP_FFMPEG)
    cap.set(cv2.CAP_PROP_BUFFERSIZE, 2)

    if not cap.isOpened():
        print('VideoCapture not opened')

    while True:
        cap.grab()
        cap.grab()
        ret, frame = cap.read()
        if not ret:
            print('frame empty')
            break
        q.put(frame)

    cap.release()


def openVideoStream():
    """
    # Server socket
    server_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    host_name  = socket.gethostname()
    host_ip = socket.gethostbyname(host_name)
    print('HOST IP:',host_ip)
    port = 12005
    socket_address = ('',port)
    print('Socket created')
    # bind the socket to the host. 
    #The values passed to bind() depend on the address family of the socket
    server_socket.bind(socket_address)
    print('Socket bind complete')
    #listen() enables a server to accept() connections
    #listen() has a backlog parameter. 
    #It specifies the number of unaccepted connections that the system will allow before refusing new connections.
    server_socket.listen(5)
    print('Socket now listening')

    data = b""
    # Q: unsigned long long integer(8 bytes)
    payload_size = struct.calcsize("Q")
    """
    # importing cascade
    faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
    cap = cv2.VideoCapture(UDP_PATH, cv2.CAP_FFMPEG)
    cap.set(cv2.CAP_PROP_BUFFERSIZE, 2)
    if not cap.isOpened():
        print('VideoCapture not opened')
        exit(-1)

    while True:
        cap.grab()
        cap.grab()
        ret, img = cap.read()

        if not ret:
            print('frame empty')
            break

        # converting image from color to grayscale 
        imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Getting corners around the face
        # 1.3 = scale factor, 5 = minimum neighbor can be detected
        faces = faceCascade.detectMultiScale(imgGray, 1.3, 5)  

        # drawing bounding box around face
        for (x, y, w, h) in faces:
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255,   0), 3)
            
        cv2.imshow('Video Stream', img)

        if cv2.waitKey(1)&0XFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    # Video Streaming test

    tr = threading.Thread(target=receiveVideoStream, daemon=True)
    tr.start()

    displayVideoStream()
